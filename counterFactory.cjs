function counterFactory(val = 0) {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let counter = val
    return {
        increment: function () {
            return counter += 1
        },
        decrement: function () {
            return counter -= 1
        }
    }
}

module.exports = counterFactory