function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    return function () {
        if (n === 0) {
            return null
        }
        const value = cb()
        while (n - 1 > 0) {
            cb()
            n--
        }
        return value === undefined ? null : value
    }
}

module.exports = limitFunctionCallCount